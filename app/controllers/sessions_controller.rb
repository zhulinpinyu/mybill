class SessionsController < ApplicationController
	def create
		user = sign_in(auth)
		flash[:notice] = "Welcome #{user.nickname}."
		redirect_to root_path
	end

	def auth
		request.env['omniauth.auth']
	end

end
