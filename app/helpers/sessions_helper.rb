module SessionsHelper
	def authentication_user
    @user = User.find(session[:user_id])
  end

  def sign_in(auth)
  	user = Authentication.find_by_provider_and_uid(auth[:provider],auth[:uid]).try(:user)
		unless user
			user = new_user(auth)
		end
		session[:user_id]=user.id
		user
  end

  def new_user(auth)
		user = User.create_auth(auth)
	end

end
