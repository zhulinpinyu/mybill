class Authentication < ActiveRecord::Base
  attr_accessible :provider, :uid, :user_id

  validates :uid, presence: true, uniqueness: {scope: :provider}
  validates :provider,presence: true, uniqueness: {scope: :user_id}
  
  belongs_to :user
end
