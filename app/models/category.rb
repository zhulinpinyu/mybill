class Category < ActiveRecord::Base
  attr_accessible :desc, :name, :parent_id

  validates_presence_of :desc, :name
  validates_uniqueness_of :name

  belongs_to :parent, class_name: "Category", foreign_key: "parent_id"
  has_many :children, class_name: "Category", foreign_key: "parent_id", dependent: :destroy

end
