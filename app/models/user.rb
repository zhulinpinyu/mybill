class User < ActiveRecord::Base
  attr_accessible :nickname

  validates :nickname, presence: true, uniqueness: true

  has_many :authentications

  accepts_nested_attributes_for :authentications

  def add_auth(auth)
  	authentications.new(
  		provider: auth[:provider],
  		uid: auth[:uid]
  	)
  end

  class << self
  	def create_auth(auth)
  		user = User.new(nickname: auth[:info][:nickname])
      user.add_auth(auth)
      user.save!
      user
  	end
  end
end
