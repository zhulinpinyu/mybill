Mybill::Application.routes.draw do
  resources :categories

  root :to => "home#index"
  match '/auth/:provider/callback', to: 'sessions#create'
end
