require 'spec_helper'

################################# can't work
describe SessionsController do
	describe "#create" do
  	context "has user" do
  		before do
  			@auth = {provider: "github",
  				uid: '12345',
  				info: {nickname: 'zlpy'}
  			}
  			controller.stub(:auth).and_return(@auth)
  			@user = double("User", id: '2')
  			User.should_receive(:create_auth).with(@auth).and_return(@user)
  		end

  		it "should login in " do
  			expect {
  				post :create 
  				session[:user_id].should == @user.id
  				response.should redirect_to root_path
  				flash[:notice].should_not be_blank
  			}.to_not change(User, :count).by(1)
  		end
  	end
  end
end
