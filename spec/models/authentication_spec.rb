require 'spec_helper'

describe Authentication do
  it { should validate_presence_of :provider }
  it { should validate_presence_of :uid }
end
