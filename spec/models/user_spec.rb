require 'spec_helper'

describe User do
  it { should validate_presence_of :nickname }
  it { should validate_uniqueness_of :nickname }

  describe "create with omniauth-github" do
  	let(:auth) {
  		{ 
  			provider: "github",
  			uid: '12345',
  			info: {nickname: 'zlpy'}
  		}
  	}

  	let(:partial_auth){
  		{ 
  			info: {nickname: 'zlpy'}
  		}
  	}

  	it "should create the user" do
  		expect {
  			User.create_auth(auth)
  		}.to change(User, :count).by(1)
  	end

  	it "should not create user" do
  		expect {
  			User.create_auth(partial_auth)
  		}.to raise_error
  	end
  end
end
